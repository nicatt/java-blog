<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Nicat Məmmədov">

    <title>Nicat.org | Ana Səhifə</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link href="${contextPath}/assets/css/header.css" rel="stylesheet">
	<style>
		div.card-text{
			line-height: 1.5em;
			max-height: 12em;
			overflow: hidden;
			text-overflow: ellipsis;
			width: 100%;
			white-space: pre-line;
		}
	</style>
  </head>

  <body>

	<!-- Navigation -->
	<%@ include file="header.jsp" %>

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

			<c:choose>
			  <c:when test="${articles.size() > 0}">
		          <h1 class="my-4">"${query}" açar sözünə uyğun gələn ${articles.size()} məqalə var.</h1>
		
				  <c:forEach var="article" items="${articles}">
		          <!-- Blog Post -->
		          <div class="card mb-4">
		            <div class="card-body">
		              <h2 class="card-title">${article.getTitle()}</h2>
		              <div class="card-text">${article.getContent()}</div>
		              <a href="${contextPath}/oxu/${article.getId()}" class="btn btn-primary mt-3 float-right">Ardını oxu &rarr;</a>
		            </div>
		            <div class="card-footer text-muted">
		              ${article.getPostDate()}
		            </div>
		          </div>
		          </c:forEach>
			  </c:when>
			  <c:otherwise>
			    <h1 class="my-4 text-center">Axtardığınız açar sözə uyğun gələn məqalə tapılmadı :(</h1>
			    <h5 class="my-4 text-center">Başqa sözləri yoxlayın.</h5>
			  </c:otherwise>
			</c:choose>
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">
          <!-- Search Widget -->
          <div class="card my-4">
            <h5 class="card-header">Search</h5>
            <div class="card-body">
            <form action="${contextPath}/search">
              <div class="input-group">
              	<input type="text" name="q" value="${query}" class="form-control" placeholder="Search for...">
	            <span class="input-group-btn">
	            	<button class="btn btn-secondary" type="submit">Go!</button>
	            </span>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
	<%@ include file="footer.jsp" %>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  </body>

</html>
