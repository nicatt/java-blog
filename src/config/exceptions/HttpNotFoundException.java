package config.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class HttpNotFoundException extends RuntimeException {
	
	String text = "Http 404 not found exception";
	
	public HttpNotFoundException(){
		
	}

	@Override
	public String toString() {
		return "HttpNotFoundException [text=" + text + "]";
	}
	
	
}