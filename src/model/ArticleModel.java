package model;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import config.exceptions.HttpNotFoundException;
import model.pojo.Article;

public class ArticleModel {
	
	private SessionFactory sessionFactory;
	
	public ArticleModel() {
		sessionFactory = new Configuration()
				.addAnnotatedClass(Article.class)
				.configure("hibernate.cfg.xml")
				.buildSessionFactory();
	}
	
	public Article getArticleById(int id){

		Article article = null;		
		Session session = sessionFactory.getCurrentSession();
		
		try {
			session.beginTransaction();
			
			article = (Article) session.get(Article.class, id);
			
			article.setContent(article.getContent().replaceAll("\n", "<br />"));
			
			session.getTransaction().commit();
		}
		catch (Exception e) {
			throw new HttpNotFoundException(); //not found
		}
		finally {
			session.close();
		}
		
		return article;
	}
	
	public List<Article> searchArticle(String keyword) {
		List<Article> list = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			session.beginTransaction();
			
			list = session.createQuery("FROM Article WHERE title LIKE concat('%',:keyword,'%') ORDER BY id DESC")
					.setParameter("keyword", keyword)
					.getResultList();
			
			session.getTransaction().commit();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());;
			throw new HttpNotFoundException(); //not found
		}
		finally {
			session.close();
		}
		
		return list;
	}
}
