package model;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.Import;

import model.pojo.Article;

public class HomeModel {
	
	private SessionFactory sessionFactory;
	
	public HomeModel() {
		sessionFactory = new Configuration()
				.addAnnotatedClass(Article.class)
				.configure("hibernate.cfg.xml")
				.buildSessionFactory();
	}
	
	public List<Article> getArticles(){
		List<Article> list = null;
		
		Session session=sessionFactory.getCurrentSession();
		
		try {
			session.beginTransaction();
			
			list = session.createQuery("FROM Article ORDER BY id DESC").getResultList();

			session.getTransaction().commit();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
		}

		return list;
	}
}
