package controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import config.exceptions.HttpNotFoundException;
import model.ArticleModel;
import model.HomeModel;
import model.pojo.Article;

@Controller
public class BlogController {

	@RequestMapping("/")
	public String home(Model model) {
		
		HomeModel homeModel = new HomeModel();
		List<Article> articles = homeModel.getArticles();

		model.addAttribute("articles", articles);
		
		return "home";
	}
	
	@RequestMapping("/oxu/{id}")
	public String article(@PathVariable(value="id") final String id, Model model) {
		int article_id = Integer.parseInt(id);
		
		ArticleModel articleModel = new ArticleModel();
		Article article = articleModel.getArticleById(article_id);

		model.addAttribute("article", article);
		
		return "article";
	}

	@RequestMapping(value="/search", method=RequestMethod.GET)
	public ModelAndView search(HttpServletRequest request, HttpServletResponse response) {
		
		String query = request.getParameter("q");
		
		if(query == null) { //q parametri yoxdursa
			throw new HttpNotFoundException();
		}
		
		ArticleModel articleModel = new ArticleModel();
		List<Article> list = articleModel.searchArticle(query);
		
		
		ModelAndView mv = new ModelAndView("search");
		mv.addObject("query", HtmlUtils.htmlEscape(query));
		mv.addObject("articles", list);
		
		System.out.println(list.size());
		
		return mv;
	}
	
	@RequestMapping("/error")
	public String error() {
		return "errors/404";
	}
}
